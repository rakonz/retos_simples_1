import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                // Imagen de arriba
                Padding(
                  padding: EdgeInsets.only(right: size.width * .6,top: size.height*0.03),
                  child: Image(
                    image: AssetImage("assets/instagram.png"), height: 40, 
                  ),
                ),
                // Imagen del centro
                Image(
                  image: AssetImage("assets/image.png"),
                ),
                // Text
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Hello!",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
                ),
                SizedBox(
                  height: 10,
                ),
                // Text
                Text(
                  "Welcome to Sales TOP A Platform To \n Manage Real Estate Needs.",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(color: Colors.grey, fontSize: 16, height: 1.4),
                ),
                SizedBox(
                  height: 60,
                ),
                // Buttons
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Color(0xff0250F2)),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(29))),
                            padding: MaterialStateProperty.all(
                                EdgeInsets.symmetric(
                                    horizontal: 45, vertical: 12))),
                        onPressed: () {},
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        )),
                    TextButton(
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(29),
                                    side: BorderSide(color: Colors.black))),
                            padding: MaterialStateProperty.all(
                                EdgeInsets.symmetric(
                                    horizontal: 45, vertical: 12))),
                        onPressed: () {},
                        child: Text(
                          "Sign Up",
                          style: TextStyle(color: Colors.black),
                        )),
                  ],
                ),
                // Text
                SizedBox(height: 70,),
                Center(child: Text("Or via social media")),
                SizedBox(height: 10,),
                // Buttons
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 100),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      TextButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xff1278f3)),
                              shape: MaterialStateProperty.all(CircleBorder()),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12))),
                          onPressed: () {},
                          child: Image.asset(
                            "assets/f.png",
                            height: 20,
                            width: 20,
                          )),
                      TextButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xffD13833)),
                              shape: MaterialStateProperty.all(CircleBorder()),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12))),
                          onPressed: () {},
                          child: Image.asset(
                            "assets/g.png",
                            height: 20,
                            width: 20,
                          )),
                      TextButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xff0076b4)),
                              shape: MaterialStateProperty.all(CircleBorder()),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.symmetric(
                                      horizontal: 12, vertical: 12))),
                          onPressed: () {},
                          child: Image.asset(
                            "assets/in.png",
                            height: 20,
                            width: 20,
                          )),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
