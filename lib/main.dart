import 'package:flutter/material.dart';
import 'package:retos_simples_1/pages/welcome_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reto simple 1',
      initialRoute: 'welcome_page',
      routes: {
        'welcome_page' : (_) => WelcomePage()
      },
    );
  }
}